from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import *


context = {}
def home(request):
	return render(request,'main/home.html',context)

def informasiku(request):
	return render(request,'main/informasiku.html',context)
