from django.test import TestCase, Client		
from django.urls import resolve
from .models import *
from .views import *

class TestDaftarBuku(TestCase):

    def test_status_url1(self):
    	response = Client().get('/story8/daftar_buku/')
    	self.assertEqual(response.status_code,200)

    def test_template(self):
    	response = self.client.get('/story8/daftar_buku/')        
    	self.assertTemplateUsed(response, 'story8/daftarbuku.html')

    def test_status_url2(self):
    	response = self.client.get('/story8/daftar_buku/hasil/?q=frozen')        
    	self.assertEqual(response.status_code,200)