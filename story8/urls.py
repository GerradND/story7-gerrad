from django.urls import path

from . import views

app_name = 'story8'

urlpatterns = [
    path('daftar_buku/', views.daftar_buku, name='daftar_buku'),
    path('daftar_buku/hasil/', views.hasil),
]