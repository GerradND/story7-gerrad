from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
import requests
import json


context = {}
def daftar_buku(request):
	return render(request,'story8/daftarbuku.html',context)

def hasil(request):
	my_request = requests.get("https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q'])
	my_content = my_request.content
	my_data = json.loads(my_content)
	return JsonResponse(my_data, safe=False)
